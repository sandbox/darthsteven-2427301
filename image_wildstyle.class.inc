<?php

/**
 * Class ImageWildstyle.
 *
 * This is a class that we inject into where the image_styles function and is
 * pretending to be an array.
 *
 * We implement the Iterator class and iterate over all normal defined styles.
 */
class ImageWildstyle implements Iterator, ArrayAccess {
  protected $defined_styles;

  protected $wild_styles;

  /**
   * Ensure that the $defined_styles property is full of default styles.
   */
  protected function initialiseStyles() {
    if (!isset($this->defined_styles)) {
      $styles = array();
      if ($cache = cache_get('image_styles', 'cache')) {
        $styles = $cache->data;
      }
      else {
        $styles = array();

        // Select the module-defined styles.
        foreach (module_implements('image_default_styles') as $module) {
          $module_styles = module_invoke($module, 'image_default_styles');
          foreach ($module_styles as $style_name => $style) {
            $style['name'] = $style_name;
            $style['label'] = empty($style['label']) ? $style_name : $style['label'];
            $style['module'] = $module;
            $style['storage'] = IMAGE_STORAGE_DEFAULT;
            foreach ($style['effects'] as $key => $effect) {
              $definition = image_effect_definition_load($effect['name']);
              $effect = array_merge($definition, $effect);
              $style['effects'][$key] = $effect;
            }
            $styles[$style_name] = $style;
          }
        }

        // Select all the user-defined styles.
        $user_styles = db_select('image_styles', NULL, array('fetch' => PDO::FETCH_ASSOC))
          ->fields('image_styles')
          ->orderBy('name')
          ->execute()
          ->fetchAllAssoc('name', PDO::FETCH_ASSOC);

        // Allow the user styles to override the module styles.
        foreach ($user_styles as $style_name => $style) {
          $style['module'] = NULL;
          $style['storage'] = IMAGE_STORAGE_NORMAL;
          $style['effects'] = image_style_effects($style);
          if (isset($styles[$style_name]['module'])) {
            $style['module'] = $styles[$style_name]['module'];
            $style['storage'] = IMAGE_STORAGE_OVERRIDE;
          }
          $styles[$style_name] = $style;
        }

        drupal_alter('image_styles', $styles);
        cache_set('image_styles', $styles);
      }
      $this->defined_styles = $styles;
    }
  }


  /**
   * Ensure that the $wild_styles property is full of wildstyles.
   */
  protected function getWildstyles() {
    if (!isset($this->wild_styles)) {
      $this->wild_styles = image_wildstyle_get_wildstyles();
    }
    return $this->wild_styles;
  }

  public function getWildstylePatterns() {
    $wildstyles = $this->getWildstyles();
    $keys = array_keys($wildstyles);

    $patterns = array();

    foreach ($keys as $key) {
      $patterns[$key] = '/' . str_replace('*', '\d+', $key) . '/';
    }

    return $patterns;
  }

  public function tameWildstyle($wildstyle_name, $wild_image_style_name) {
    $wildstyles = $this->getWildstyles();
    if (isset($wildstyles[$wildstyle_name])) {
      $wildstyle = $wildstyles[$wildstyle_name];

      $base_image_style = $this[$wildstyle['base_style']];
      $base_image_style['name'] = $wild_image_style_name;

      // Get the tokens from the $wild_image_style_name:
      $pattern = '/' . str_replace('*', '(\d+)', $wildstyle_name) . '/';
      $matches = array();
      preg_match($pattern, $wild_image_style_name, $matches);

      // Initialise the tokens.
      $tokens = array();
      for ($i = 1;$i < count($matches);$i++) {
        $tokens["[url_$i]"] = $matches[$i];
      }

      // @TODO: allow tokens from the original image style.

      // Now we can compute some more tokens.
      foreach ($wildstyle['tokens'] as $token => $expression) {
        $token_expression = str_replace(array_keys($tokens), array_values($tokens), $expression);
        $tokens[$token] = eval('return ' . $token_expression. ';');
      }

      // Now push any overrides back into the image style.
      foreach ($wildstyle['overrides'] as $override) {
        $value = eval('return ' . str_replace(array_keys($tokens), array_values($tokens), $override['expression']) . ';');
        drupal_array_set_nested_value($base_image_style['effects'], $override['location'], $value);
      }

      // Finally, we can return the computed image style.
      return $base_image_style;
    }
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Return the current element
   * @link http://php.net/manual/en/iterator.current.php
   * @return mixed Can return any type.
   */
  public function current() {
    $this->initialiseStyles();
    return current($this->defined_styles);
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Return the key of the current element
   * @link http://php.net/manual/en/iterator.key.php
   * @return mixed scalar on success, or null on failure.
   */
  public function key() {
    $this->initialiseStyles();
    return key($this->defined_styles);
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Move forward to next element
   * @link http://php.net/manual/en/iterator.next.php
   * @return void Any returned value is ignored.
   */
  public function next() {
    $this->initialiseStyles();
    return next($this->defined_styles);
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Rewind the Iterator to the first element
   * @link http://php.net/manual/en/iterator.rewind.php
   * @return void Any returned value is ignored.
   */
  public function rewind() {
    $this->initialiseStyles();
    return reset($this->defined_styles);
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Checks if current position is valid
   * @link http://php.net/manual/en/iterator.valid.php
   * @return boolean The return value will be casted to boolean and then evaluated.
   * Returns true on success or false on failure.
   */
  public function valid() {
    $this->initialiseStyles();
    return isset($this->defined_styles[$this->key()]);
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Whether a offset exists
   * @link http://php.net/manual/en/arrayaccess.offsetexists.php
   * @param mixed $offset <p>
   * An offset to check for.
   * </p>
   * @return boolean true on success or false on failure.
   * </p>
   * <p>
   * The return value will be casted to boolean if non-boolean was returned.
   */
  public function offsetExists($offset) {
    $this->initialiseStyles();
    if (isset($this->defined_styles[$offset])) {
      return isset($this->defined_styles[$offset]);
    }
    $patterns = $this->getWildstylePatterns();
    // Keys of the wildstyles array are patterns, see if any match.
    foreach ($patterns as $name => $pattern) {
      if (preg_match($pattern, $offset)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Offset to retrieve
   * @link http://php.net/manual/en/arrayaccess.offsetget.php
   * @param mixed $offset <p>
   * The offset to retrieve.
   * </p>
   * @return mixed Can return all value types.
   */
  public function offsetGet($offset) {
    $this->initialiseStyles();
    if (isset($this->defined_styles[$offset])) {
      return $this->defined_styles[$offset];
    }
    $patterns = $this->getWildstylePatterns();
    // Keys of the wildstyles array are patterns, see if any match.
    foreach ($patterns as $name => $pattern) {
      if (preg_match($pattern, $offset)) {
        return $this->tameWildstyle($name, $offset);
      }
    }
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Offset to set
   * @link http://php.net/manual/en/arrayaccess.offsetset.php
   * @param mixed $offset <p>
   * The offset to assign the value to.
   * </p>
   * @param mixed $value <p>
   * The value to set.
   * </p>
   * @return void
   */
  public function offsetSet($offset, $value) {
    throw new Exception('We really should not be set');
  }

  /**
   * (PHP 5 &gt;= 5.0.0)<br/>
   * Offset to unset
   * @link http://php.net/manual/en/arrayaccess.offsetunset.php
   * @param mixed $offset <p>
   * The offset to unset.
   * </p>
   * @return void
   */
  public function offsetUnset($offset) {
    throw new Exception('We really should not be unset');
  }
}